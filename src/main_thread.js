const link = document.createElement(`a`);
link.href = blobUrl;
link.download = fileName;

link.style.position = `fixed`;
link.style.top = link.style.left = `0`;

document.body.append(link);
link.click();
link.remove();
