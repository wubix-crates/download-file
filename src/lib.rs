use js_sys::{Array, Uint8Array};
use librust::{env::args_os, ffi::OsString, fs::write, path::PathBuf, println, process};
use wasm_bindgen::prelude::*;
use web_sys::{File, Url};

#[derive(Debug)]
struct FileToDownload {
    path: PathBuf,
    rename: Option<OsString>,
}

const MAIN_THRAD_CODE: &str = include_str!("./main_thread.js");

#[wasm_bindgen]
pub async fn start() {
    let mut files: Vec<FileToDownload> = Vec::new();

    let mut arguments = args_os().skip(1);
    while let Some(argument) = arguments.next() {
        if argument == "--help" || argument == "-h" {
            println!(
                "`download-file` lets you download files from Wubix to your host machine.\n\n\

                 USAGE:\n    \
                    download-file <files>...\n\n\

                 OPTIONS:\n    \
                     --as    When specified right after a file path, changes the downloaded file's \
                     name. If omitted for a file, the basename of the file path is used.\n\n\

                 EXAMPLES:\n    \
                     download-file /etc/passwd --as users /etc/fstab\n\t\
                         Downloads `/etc/passwd` as `users` and `/etc/fstab` as `fstab`."
            )
            .await;
            process::exit(0);
        }

        if argument == "--as" {
            let rename_to = if let Some(rename_to) = arguments.next() {
                rename_to
            } else {
                bunt::eprintln!("{$red}error:{/$} `--as` must be followed by a new name").await;
                process::exit(2);
            };

            let last_file = if let Some(file) = files.last_mut() {
                file
            } else {
                bunt::eprintln!("{$red}error:{/$} `--as` must follow a file path").await;
                process::exit(2);
            };

            if last_file.rename.is_some() {
                bunt::eprintln!("{$red}error:{/$} `--as` was repeated for the same file").await;
                process::exit(2);
            }

            last_file.rename = Some(rename_to);
            continue;
        }

        files.push(FileToDownload {
            path: PathBuf::from(argument),
            rename: None,
        });
    }

    if files.is_empty() {
        bunt::eprintln!("{$red}error:{/$} no file to download").await;
        process::exit(1);
    }

    for FileToDownload { path, rename } in files {
        let bytes = match librust::fs::read(&path).await {
            Ok(bytes) => bytes,
            Err(error) => {
                bunt::eprintln!("{$red}error:{/$} {}", error).await;
                continue;
            }
        };
        let uint8array = Uint8Array::from(bytes.as_slice());
        let file_name = rename
            .as_deref()
            .unwrap_or_else(|| path.file_name().unwrap())
            .to_string_lossy();
        let file = File::new_with_u8_array_sequence(&Array::of1(&uint8array), &file_name).unwrap();
        let blob_url = Url::create_object_url_with_blob(&file).unwrap();

        write(
            "/dev/main_thread",
            format!(
                "const fileName = {file_name:?};\
                 const blobUrl = {blob_url:?};\
                 {code}",
                file_name = file_name,
                blob_url = blob_url,
                code = MAIN_THRAD_CODE,
            ),
        )
        .await
        .unwrap();
    }
}
